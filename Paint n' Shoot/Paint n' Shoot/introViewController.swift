//
//  introViewController.swift
//  Paint n' Shoot
//
//  Created by Angela Lambert on 5/8/20.
//  Copyright © 2020 Social Swiftancing. All rights reserved.
//

import UIKit
import Foundation

class introViewController: UIViewController {
    
    @IBOutlet var viewOutlet: UIView!
    @IBOutlet weak var enterOutlet: UIButton!
    
    @IBAction func enterAction(_ sender: Any) {
        let tabBarController = storyboard?.instantiateViewController(withIdentifier: "tabBarController") as! tabBarController
        
        tabBarController.modalPresentationStyle = .fullScreen
        self.present(tabBarController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enterOutlet.layer.cornerRadius = 5
        
    }
}
