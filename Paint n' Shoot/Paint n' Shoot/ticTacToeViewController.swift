//
//  ticTacToeViewController.swift
//  Paint n' Shoot
//
//  Created by Angela Lambert on 5/5/20.
//  Copyright © 2020 Social Swiftancing. All rights reserved.
//

import UIKit
import Foundation

class ticTacToeViewController: UIViewController {
    var color = UIColor.black
    var team = 1
    var game = TicTacToe()
    var status = "Playing"
    var arrows: [UIButton]! = []
    
    @IBOutlet var buttonOutlets: [UIButton]!
    @IBOutlet weak var shooterOutlet: UIButton!
    @IBOutlet weak var shooterViewStack: UIView!
    @IBOutlet weak var ticTacToeViewStack: UIView!
    @IBOutlet weak var choiceOutlet: UIView!
    @IBOutlet weak var turnOutlet: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(rotatedView(_:)))
        shooterViewStack.addGestureRecognizer(rotate)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        shooterViewStack.addGestureRecognizer(longPress)
        
        ticTacToeViewStack.bringSubviewToFront(shooterViewStack)
    }
    
    func alert_view() {
        let alertController = UIAlertController(title: "Game Over", message: "\(status)", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "New Game", style: .default, handler: {(action) -> Void in
            self.newGame()
        }))
        
        self.present(alertController, animated: true)
    }
    
    func newGame() {
        team = 1
        game.board = Array(repeating: Array(repeating: 0, count: 3), count: 3)
        
        for button in buttonOutlets {
            button.layer.backgroundColor = UIColor.white.cgColor
            button.setTitle("", for: .normal)
        }
        
        for arrow in arrows {
            arrow.removeFromSuperview()
        }
    }
    
    @objc func rotatedView(_ sender: UIRotationGestureRecognizer) {
        guard sender.view != nil else { return }
        
        if sender.state == .began || sender.state == .changed {
           sender.view?.transform = sender.view!.transform.rotated(by: sender.rotation)
           sender.rotation = 0
        }
    }
    
    var start: NSDate?
    
    @objc func longPress(_ sender: UILongPressGestureRecognizer){
        if sender.state == .began {
            start = NSDate()
        }
        
        if sender.state == .ended{
            let duration = NSDate().timeIntervalSince(start! as Date)
            shoot(duration)
        }
    }
    
    func shoot(_ pwr: Double){
        let newButton = UIButton(frame: shooterOutlet.frame)
        newButton.tintColor = shooterOutlet.tintColor;
        newButton.alpha = 1.0
        
        newButton.setImage(UIImage(systemName: "triangle.fill"), for: .normal)
        arrows.append(newButton)
        
        choiceOutlet.addSubview(newButton)
        let newFrame = calcNewLocation(pwr, choiceOutlet.frame)
        UIView.animate(withDuration: 0.3, animations: {newButton.frame = newFrame})
    }
    
    func calcNewLocation(_ pwr: Double, _ canvasFrame: CGRect) -> CGRect {
        var angle = atan2f(Float(shooterViewStack.transform.b), Float(shooterViewStack.transform.a)) // radians swift
        
        angle = angle * (Float(180) / Float.pi ) // degrees swift
        let angleDeg = 90.0 - angle // converting to unit circle
        angle = angleDeg * (Float.pi / 180.0) // radians in unit circle
        let dur = (pwr > 3.0) ? 3.0 : pwr // press longer than 3 should be = to max
        let r = getPowerLength(dur, angle, canvasFrame)
        
        var newX = 0.0
        var newY = 0.0

        if (angleDeg < 90.0 ){  // First quadrant
            let x = r * Double(cos(angle))
            let y = r * Double(sin(angle))
                
            newX = Double(shooterOutlet.frame.origin.x) + x
            newY = Double(shooterOutlet.frame.origin.y) - y
            
        } else if (angleDeg == 90.0){ // no movement
            newX = Double(shooterOutlet.frame.origin.x)
            newY = Double(shooterOutlet.frame.origin.y) - r
        
        } else if (angleDeg > 90.0 && angleDeg <= 180){ // Second quadrant
            let x = r * Double(cos(angle)) // negative
            let y = r * Double(sin(angle))
                
            newX = Double(shooterOutlet.frame.origin.x) + x
            newY = Double(shooterOutlet.frame.origin.y) - y
        
        } else if (angleDeg > 180){ // Third quadrant
            let x = r * Double(cos(angle)) // negative
            let y = r * Double(sin(angle)) // negative
                
            newX = Double(shooterOutlet.frame.origin.x) + x
            newY = Double(shooterOutlet.frame.origin.y) - y
        }
        
        evalButton(newX, y: newY)
        
            //print("newX: \(newX), newY: \(newY)")
        return CGRect(x: newX, y: newY, width: Double(shooterOutlet.frame.width), height: Double(shooterOutlet.frame.width))
    }
        
    func getPowerLength(_ dur: Double, _ theta: Float, _ frame: CGRect) -> Double {
        let per = dur / 3.0
        var maxLen = 0.0
            
        // First getting max length
        if((theta >= 315 && theta <= 45) || (theta >= 135 && theta <= 225)){
            maxLen = Double(frame.maxX - shooterOutlet.frame.origin.x)
        } else {
            maxLen = Double(shooterOutlet.frame.origin.y)
        }
            
        //print("dur: \(dur)")
        return maxLen * per
    }
        
    func powerAnimator() -> Double{
        return 0.0
    }
    
    func evalButton(_ x: Double, y: Double) {
        for button in buttonOutlets {
            let multiplier = button.tag
            
            let startX = Int(button.frame.origin.x)
            let startY = Int(button.frame.origin.y)
            let endY = startY + (188 * multiplier)
            
            print(startX)
            print(endY)
            
            if Int(x) >= startX && Int(x) <= (startX + 102){
                if Int(y) >= startY && Int(y) <= (endY) {
                    if team == 1 && button.currentTitle != "2" {
                        button.layer.backgroundColor = UIColor.systemBlue.cgColor
                        button.setTitle("1", for: .normal)
                        
                        let spotX = startX/124
                        let spotY = button.tag - 1
                        
                        game.select(1, spotX, spotY)
                        
                    } else if team == 2 && button.currentTitle != "1" {
                        button.layer.backgroundColor = UIColor.systemPink.cgColor
                        button.setTitle("2", for: .normal)
                        
                        let spotX = startX/124
                        let spotY = button.tag - 1
                        
                        game.select(2, spotX, spotY)
                    }
                    
                    button.setTitleColor(.black, for: .normal)
                    break
                }
            }
        }
        
        updateViewFromGame()
    }
    
    func updateViewFromGame() {
        if team == 1 {
            team = 2
            turnOutlet.text = "Player 2's Turn"
        } else {
            team = 1
            turnOutlet.text = "Player 1's Turn"
        }
        
        if game.checkForWinHorizontal() != 0 {
            let winner = game.checkForWinHorizontal()
            status = "Player \(winner) Wins!!!"
            alert_view()
        }
        
        if game.checkForWinVertical() != 0 {
            let winner = game.checkForWinVertical()
            status = "Player \(winner) Wins!!!"
            alert_view()
        }
        
        if game.checkForWinDiagonal() != 0 {
            let winner = game.checkForWinDiagonal()
            status = "Player \(winner) Wins!!!"
            alert_view()
        }
        
        if game.checkForDraw() == true {
            status = "It's a Draw!"
            alert_view()
        }
    }
        
}
