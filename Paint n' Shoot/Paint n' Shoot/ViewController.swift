//
//  ViewController.swift
//  Paint n' Shoot
//
//  Created by Sharon Halevi on 4/12/20.
//  Copyright © 2020 Social Swiftancing. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {    
    @IBOutlet weak var shooterOutlet: UIButton!
   // @IBOutlet weak var shapeControl: UISegmentedControl!
    @IBOutlet weak var shooterViewStack: UIView!
    @IBOutlet weak var canvasOutlet: UIView!
    @IBOutlet weak var clearOutlet: UIButton!
    
    var arrows: [UIButton]! = []
    var shape = -1
    @IBOutlet var colorsOutlet: [UIButton]!
    @IBOutlet weak var SaveOutlet: UIButton!
    @IBOutlet var shapeOutlet: [UIButton]!
    
    @IBAction func clearAction(_ sender: Any) {
        clear()
    }
    
    @IBAction func SaveAction(_ sender: Any) {
        let image = canvasOutlet.asImage()
        
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    @IBAction func colorAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 0:
            shooterOutlet.tintColor = UIColor.red
        case 1:
            shooterOutlet.tintColor = UIColor.orange
        case 2:
            shooterOutlet.tintColor = UIColor.systemYellow
        case 3:
            shooterOutlet.tintColor = UIColor.systemGreen
        case 4:
            shooterOutlet.tintColor = UIColor.blue
        case 5:
            shooterOutlet.tintColor = UIColor.purple
        default:
            shooterOutlet.tintColor = UIColor.black
        }
    }
    
    @IBAction func shapeAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 0:
            shooterOutlet.setImage(UIImage(systemName: "circle.fill"), for: .normal)
            shape = 0
        case 1:
            shooterOutlet.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            shape = 1
        case 2:
             shooterOutlet.setImage(UIImage(systemName: "star.fill"), for: .normal)
            shape = 2
        case 3:
            shooterOutlet.setImage(UIImage(systemName: "square.fill"), for: .normal)
            shape = 3
        default:
            shooterOutlet.setImage(UIImage(systemName: "location.north.fill"), for: .normal)
            shape = -1
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SaveOutlet.layer.cornerRadius = 5
        clearOutlet.layer.cornerRadius = 5
        
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(rotatedView(_:)))
        shooterViewStack.addGestureRecognizer(rotate)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        shooterViewStack.addGestureRecognizer(longPress)
    }
    
    @objc func rotatedView(_ sender: UIRotationGestureRecognizer) {
        guard sender.view != nil else { return }
        
        if sender.state == .began || sender.state == .changed {
           sender.view?.transform = sender.view!.transform.rotated(by: sender.rotation)
           sender.rotation = 0
        }
    }
    
    var start: NSDate?
    
    @objc func longPress(_ sender: UILongPressGestureRecognizer){
       
        if sender.state == .began {
            start = NSDate()
        }
        
        if sender.state == .ended{
            let duration = NSDate().timeIntervalSince(start! as Date)
            shoot(duration)
        }
    }
    
    func clear() {
        for arrow in arrows {
            arrow.removeFromSuperview()
        }
    }
    
    /******** Animation for shooting **********/
    
    func shoot(_ pwr: Double){
        let newButton = UIButton(frame: shooterOutlet.frame)
        newButton.tintColor = shooterOutlet.tintColor;
        newButton.alpha = 1.0
        switch shape {
        case 0: //circle
            newButton.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        case 1: //heart
            newButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        case 2: //star
            newButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        case 3: //square
            newButton.setImage(UIImage(systemName: "square.fill"), for: .normal)
        default: //location
            newButton.setImage(UIImage(systemName: "location.north.fill"), for: .normal)
        }

        arrows.append(newButton)
        canvasOutlet.addSubview(newButton)
        let newFrame = calcNewLocation(pwr, canvasOutlet.frame)
        UIView.animate(withDuration: 0.3, animations: {newButton.frame = newFrame})
    }
    
    /** Calculating new X/Y Coords **/
    func calcNewLocation(_ pwr: Double, _ canvasFrame: CGRect) -> CGRect{
        //print("shooter: (\(shooterOutlet.frame.origin.x), \(shooterOutlet.frame.origin.y))")
        var angle = atan2f(Float(shooterViewStack.transform.b), Float(shooterViewStack.transform.a)) // radians swift
        angle = angle * (Float(180) / Float.pi ) // degrees swift
        let angleDeg = 90.0 - angle // converting to unit circle
        angle = angleDeg * (Float.pi / 180.0) // radians in unit circle
        let dur = (pwr > 3.0) ? 3.0 : pwr // press longer than 3 should be = to max
        let r = getPowerLength(dur, angle, canvasFrame)
        var newX = 0.0
        var newY = 0.0
      
        //print("angle in deg in unit cir: \(angleDeg)")
       // print("r: \(r)")
        

        if (angleDeg < 90.0 ){  // First quadrant
            let x = r * Double(cos(angle))
            let y = r * Double(sin(angle))
            
            newX = Double(shooterOutlet.frame.origin.x) + x
            newX = (newX >= Double(canvasFrame.maxX)) ? Double(canvasFrame.maxX) - 20 : newX
            newY = Double(shooterOutlet.frame.origin.y) - y
            newY = (newY <= 0) ? 10 : newY
            
        } else if(angleDeg == 90.0){ // no movement
            newX = Double(shooterOutlet.frame.origin.x)
            newY = Double(shooterOutlet.frame.origin.y) - r
            
        } else if(angleDeg > 90.0 && angleDeg <= 180){ // Second quadrant
            let x = r * Double(cos(angle)) // negative
            let y = r * Double(sin(angle))
            
            newX = Double(shooterOutlet.frame.origin.x) + x
            newX = (newX <= 0) ? 10 : newX
            newY = Double(shooterOutlet.frame.origin.y) - y
            newY = (newY <= 0) ? 10 : newY
            
        } else if(angleDeg > 180){ // Third quadrant
            let x = r * Double(cos(angle)) // negative
            let y = r * Double(sin(angle)) // negative
            
            newX = Double(shooterOutlet.frame.origin.x) + x
            newX = (newX <= 0) ? 10 : newX
            newY = Double(shooterOutlet.frame.origin.y) - y
            newY = (newY >= Double(canvasFrame.maxY)) ? Double(canvasFrame.maxY) - 10 : newY
        }
    
        
        //print("newX: \(newX), newY: \(newY)")
        return CGRect(x: newX, y: newY, width: Double(shooterOutlet.frame.width), height: Double(shooterOutlet.frame.width))
    }
    
    func getPowerLength(_ dur: Double, _ theta: Float, _ frame: CGRect) -> Double {
        let per = dur / 3.0
        var maxLen = 0.0
        // First getting max length
        if((theta >= 315 && theta <= 45) || (theta >= 135 && theta <= 225)){
            maxLen = Double(frame.maxX - shooterOutlet.frame.origin.x)
        }else{
            maxLen = Double(shooterOutlet.frame.origin.y)

        }
        
        //print("dur: \(dur)")
        return maxLen * per
    }
    
    
    func powerAnimator() -> Double{
        return 0.0
    }
}

extension UIView {
    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}

