//
//  TicTacToe.swift
//  Paint n' Shoot
//
//  Created by Sharon Halevi on 5/3/20.
//  Copyright © 2020 Social Swiftancing. All rights reserved.
//

import Foundation

class TicTacToe {
    
    var team1:Int = 1
    var team2:Int = 2
    var board = Array(repeating: Array(repeating: 0, count: 3), count: 3)
    
    /*When a spot on the board is chosen, that spot is
     assigned the number of the team taking the turn*/
    func select(_ team: Int, _ spotX:Int, _ spotY:Int){
        if board[spotX][spotY] == 0{
            board[spotX][spotY] = team
        }
    }
    
    /* Checks for 3 of the same nonzero number in a row,
     returning the number of the winning team if there is
     one, and 0 otherwise */
    func checkForWinVertical() -> Int {
        for i in 0..<3{
            if board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != 0{
                return board[i][0]
            }
        }
        return 0
    }
    
    func checkForWinHorizontal() -> Int {
        for i in 0..<3{
            if board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != 0{
                return board[0][i]
            }
        }
        return 0
    }
    
    func checkForWinDiagonal() -> Int {
        if board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != 0{
            return board[0][0]
        }
        
        if board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] != 0{
            return board[2][0]
        }
        return 0
    }
    
    func checkForDraw() -> Bool {
        if checkForWinDiagonal() == 0 && checkForWinVertical() == 0 && checkForWinHorizontal() == 0 {
            
            for i in 0..<3{
                for j in 0..<3{
                    if board[i][j] == 0 {
                        return false
                    }
                }
            }
            
        }
        
        return true
    }
}
